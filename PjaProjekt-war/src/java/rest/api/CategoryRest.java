/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.api;

import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import src.api.CategoryManage;
import src.models.Category;


/**
 *
 * @author fxdp
 */
@Path("/category")
public class CategoryRest {
    
    
    @EJB
    private CategoryManage catManage;
    
    @GET
    @Path("getCategories")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTools() {
        List<Category> cats = catManage.getCategories();

        JSONObject jObject = new JSONObject();
        JSONArray jArray = new JSONArray();
        try {
            
            for (Category c : cats) {
                JSONObject catJSON = new JSONObject();
                catJSON.put("id", c.getId());
                catJSON.put("name", c.getName());               
                jArray.put(catJSON);
            }
            jObject.put("CategoryList", jArray);
            
        } catch (JSONException jse) {

        }
        return jArray.toString();
    }
    
}
