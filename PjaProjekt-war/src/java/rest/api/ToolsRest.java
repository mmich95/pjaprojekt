/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.api;

import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import src.api.ToolManage;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
@Path("/tools")
public class ToolsRest {

    @EJB
    private ToolManage toolManage;

    @GET
    @Path("getTools")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTools() {
        List<Tool> tools = toolManage.getTools();

        JSONObject jObject = new JSONObject();
        JSONArray jArray = new JSONArray();
        try {
            
            for (Tool t : tools) {
                JSONObject toolJSON = new JSONObject();
                toolJSON.put("id", t.getId());
                toolJSON.put("name", t.getName());
                toolJSON.put("info", t.getInfo());
                toolJSON.put("price", t.getPriceForDay());
                toolJSON.put("image", t.getImage());
                jArray.put(toolJSON);
            }
            jObject.put("ToolsList", jArray);
            
        } catch (JSONException jse) {

        }
        return jArray.toString();
    }

}
