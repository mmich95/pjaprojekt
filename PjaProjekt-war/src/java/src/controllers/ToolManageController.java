/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import src.api.CategoryManage;
import src.api.ToolManage;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
@RequestScoped
@Named("toolManageCtl")
public class ToolManageController {

    @EJB
    private ToolManage toolManage;

    @EJB
    private CategoryManage catManage;
    private String name;
    private String info;
    private byte[] image = new byte[100];
    private int priceForDay;
    private Part uploadedFile;
    private long categoryId;

    public String saveForm() throws IOException {

        image = new byte[(int) uploadedFile.getSize()];
        DataInputStream dataIs = new DataInputStream(uploadedFile.getInputStream());
        dataIs.readFully(image);

        toolManage.add(name, info, image, catManage.getById(categoryId), priceForDay);
        return "tool-manage";
    }

    public List<Tool> getTools() {
        return toolManage.getTools();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getPriceForDay() {
        return priceForDay;
    }

    public void setPriceForDay(int priceForDay) {
        this.priceForDay = priceForDay;
    }

    public Part getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(Part uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
}
