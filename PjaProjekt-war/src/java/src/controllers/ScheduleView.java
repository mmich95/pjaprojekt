/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

/**
 *
 * @author fxdp
 */
import java.io.Serializable;
import java.util.Calendar;

import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;

import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import src.api.ReservationManage;
import src.api.ToolManage;
import src.models.Tool;

@ManagedBean
@ViewScoped
public class ScheduleView implements Serializable {

    private ScheduleModel eventModel;

    private ScheduleModel lazyEventModel;

    private ScheduleEvent event = new DefaultScheduleEvent();
    private Tool showTool;
    
    @EJB
    ReservationManage resManage;
    
    @EJB
    ToolManage toolManage;

    @PostConstruct
    public void init() {
        eventModel = new DefaultScheduleModel();
        

    }

    public String show(long id) {
        
        
        return "reservationSchedule";
    }

    public ScheduleModel getEventModel() {
        
        return eventModel;
    }
    
    public ScheduleModel getEventModelByTool(Tool tool) {
        eventModel.clear();
        resManage.getReservationsByTool(tool)
                
                .stream().forEach((r) -> {
                    r.getDateSince().add(Calendar.HOUR_OF_DAY, 8);
                    r.getDateTo().add(Calendar.HOUR_OF_DAY, 8);
                    eventModel.addEvent(new DefaultScheduleEvent("Borrowed",
                            r.getDateSince().getTime(),
                            r.getDateTo().getTime()));
                    r.getDateSince().add(Calendar.HOUR_OF_DAY, -8);
                    r.getDateTo().add(Calendar.HOUR_OF_DAY, -8);
                    
                });
        return eventModel;
    }

    

    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public void addEvent(ActionEvent actionEvent) {
        if (event.getId() == null) {
            eventModel.addEvent(event);
        } else {
            eventModel.updateEvent(event);
        }

        event = new DefaultScheduleEvent();
    }

    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }

    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
