/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
@ApplicationScoped
@Named("scheduleCtl")
public class ScheduleController {
    
    private Tool showTool;
    
    public String show(Tool tool) {
        this.showTool = tool;
        return "reservationSchedule";
    }

    public Tool getShowTool() {
        return showTool;
    }
    
    public String back() {
        return "tool-detail";
    }
    
}
