/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

/**
 *
 * @author fxdp
 */

 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;
 
@ManagedBean(name = "dfView")
public class DFView {
         
    public void showMessageAfterConfirm() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Úspěch", "Výpůjčka přidána do košíku");         
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
    public void showMessageAfterOrder() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Úspěch", "Vaše objednávka byla dokončena");         
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
}