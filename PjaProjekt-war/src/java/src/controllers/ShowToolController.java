/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import src.api.ReservationManage;

import src.models.Tool;

/**
 *
 * @author fxdp
 */
@SessionScoped
@Named
public class ShowToolController implements Serializable {

    private Tool showTool;
    private Calendar sinceDate;
    private Calendar toDate;
    private String sinceDateString;
    private String toDateString;
    private int totalPrice = 0;
    private String actual = "";

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    @EJB
    private ReservationManage resManage;

    @Inject
    CartController cartCtl;

    ShowToolController() {

    }

    public String show(Tool tool) throws ParseException {
        this.showTool = tool;
        /*
        this.sinceDate = resManage.getSinceDateByTool(tool);
        this.toDate = resManage.getToDateByTool(tool);
        
         */
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");

        this.sinceDateString = dateFormat.format(resManage.getSinceDateByTool(tool).getTime());
        this.toDateString = dateFormat.format(resManage.getToDateByTool(tool).getTime());
        if (resManage.getToDateByTool(tool).equals(new GregorianCalendar(2999, 0, 31))) {
            this.toDateString = "";
        }

        if (resManage.isRented(tool)) {
            this.actual = "Půjčeno";
        } else {
            this.actual = "Volné";
        }

        return "tool-detail";
    }

    public String submit() throws ParseException {
        /*
        long resId = resManage.add(this.sinceDate, this.toDate, this.showTool, this.totalPrice);
        cartCtl.addReservation(resManage.getById(resId));
        System.out.println("Velikost kosiku " + cartCtl.getReservations().size());
        this.sinceDate = resManage.getSinceDateByTool(this.showTool);
        this.toDate = resManage.getToDateByTool(this.showTool);
        if (resManage.isRented(this.showTool)) {
            this.actual = "Půjčeno";
        } else {
            this.actual = "Volné";
        }
         */
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        Calendar sinceDateTmp = Calendar.getInstance();
        sinceDateTmp.setTime(dateFormat.parse(this.sinceDateString));
        
        Calendar toDateTmp = Calendar.getInstance();
        toDateTmp.setTime(dateFormat.parse(this.toDateString));
        
        long resId = resManage.add(sinceDateTmp, toDateTmp, this.showTool, this.totalPrice);
        cartCtl.addReservation(resManage.getById(resId));
        this.sinceDateString = dateFormat.format(resManage.getSinceDateByTool(this.showTool).getTime());
        this.toDateString = dateFormat.format(resManage.getToDateByTool(this.showTool).getTime());
        if (resManage.getToDateByTool(this.showTool).equals(new GregorianCalendar(2999, 0, 31))) {
            this.toDateString = "";
        }
        
        if (resManage.isRented(this.showTool)) {
            this.actual = "Půjčeno";
        } else {
            this.actual = "Volné";
        }
        
        return "tool-detail";

    }

    public Tool getShowTool() {
        return showTool;
    }

    public void setShowTool(Tool showTool) {
        this.showTool = showTool;
    }

    public String getSinceDateInString() {
        /*
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        dateFormat.setTimeZone(sinceDate.getTimeZone());
        return dateFormat.format(sinceDate.getTime());
         */
        return this.sinceDateString;

    }

    public String getToDateInString() {
        /*
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        if (toDate.equals(new GregorianCalendar(2999, 0, 31))) {
            return "";
        }
        dateFormat.setTimeZone(toDate.getTimeZone());
        return dateFormat.format(toDate.getTime());
         */

        return this.toDateString;

    }

    public void setSinceDateInString(String sinceDate) throws ParseException {
        /*
        if (sinceDate == null) {

        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);

            this.sinceDate.setTime(dateFormat.parse(sinceDate));

        }
         */
        if (sinceDate == null) {

        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(sinceDate));
            dateFormat = new SimpleDateFormat("dd.MM.yy");
            this.sinceDateString = dateFormat.format(cal.getTime());

        }
    }

    public void setToDateInString(String toDate) throws ParseException {
        /*
        if (toDate == null) {

        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);

            this.toDate.setTime(dateFormat.parse(toDate));

        }
         */

        if (toDate == null) {

        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(toDate));
            dateFormat = new SimpleDateFormat("dd.MM.yy");
            this.toDateString = dateFormat.format(cal.getTime());

        }

    }

    public String getTotalPriceInString() throws ParseException {
        /*     
        if (toDate.equals(new GregorianCalendar(2999, 0, 31))) {
            return "";
        } else {
            long end = this.toDate.getTimeInMillis();
            long start = this.sinceDate.getTimeInMillis();
            long total = TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
            this.totalPrice = (int) (total * this.showTool.getPriceForDay());
            return String.valueOf(total * this.showTool.getPriceForDay()) + " CZK";
        }
         */

        if (this.toDateString.equals("")) {
            return "";
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(this.sinceDateString));
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dateFormat.parse(this.toDateString));

            long end = cal1.getTimeInMillis();
            long start = cal.getTimeInMillis();
            long total = TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
            this.totalPrice = (int) (total * this.showTool.getPriceForDay());
            return String.valueOf(total * this.showTool.getPriceForDay()) + " CZK";
        }

    }

    public Calendar getSinceDate() {
        return sinceDate;
    }

    public void setSinceDate(Calendar sinceDate) {
        this.sinceDate = sinceDate;
    }

    public Calendar getToDate() {
        return toDate;
    }

    public void setToDate(Calendar toDate) {
        this.toDate = toDate;
    }

}
