/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import javax.inject.Named;
import src.api.CategoryManage;
import src.api.ToolManage;
import src.models.Tool;
import src.models.Category;

/**
 *
 * @author fxdp
 */
@ApplicationScoped
@Named("toolListCtl")
public class ToolListController {

    @EJB
    private ToolManage toolManage;
    
    @EJB
    private CategoryManage catManage;

    private long catId = 0;

    public String showAll() {
        System.out.println("showAll");
        this.catId = 0;
        System.out.println(catId);
        return "tool-list.xhtml";
    }

    public String showByCategory(Category cat) {
            catId = cat.getId();
        return "tool-list.xhtml";
    }

    public List<Tool> getTools() {
        System.out.println("Vracim toolu:" + catId);
        if (catId == 0) {
            return toolManage.getTools();
        } else {
            return toolManage.getToolsByCategory(catManage.getById(catId));
        }

    }

}
