/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import src.api.CartManage;
import src.models.Reservation;

/**
 *
 * @author fxdp
 */
@SessionScoped
@Named("cartCtl")
public class CartController implements Serializable {
    
    @EJB
    CartManage cartManage;
    
    public List<Reservation> getReservations(){
        
        
        return cartManage.getReservations();
    }
    
    public void addReservation(Reservation r){
        cartManage.addReservation(r);
    }
    
    public void deleteReservation(Reservation r){
        cartManage.removeReservation(r);
    }
    
    
    public String confirm(){
        
        return "add-user-data";
    }
    
    
    public String discard(){
        cartManage.discard();
        return "cart";
    }
    
    public void afterOrder(){
        cartManage.afterOrder();
        
    }
    
}
