/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import src.api.OrderManage;
import src.api.UserManage;

/**
 *
 * @author fxdp
 */
@SessionScoped
@Named("orderCtl")
public class OrderController implements Serializable {

    @EJB
    private OrderManage orderManage;

    @EJB
    private UserManage userManage;   
       

    @Inject
    CartController cartCtl;

    private String firstName;
    private String secondName;
    private String telNumber;
    private String email;

    public String submit() {
        long userId = userManage.addUser(null,null,this.firstName, this.secondName, this.telNumber, this.email);
        int totalPrice = 0;

        orderManage.addOrder(
                userManage.getUserById(userId), cartCtl.getReservations(),
                cartCtl.getReservations().stream()
                        .map((r) -> r.getPriceTotal())
                        .reduce(totalPrice, Integer::sum));
        cartCtl.afterOrder();
        
        return "tool-list";

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    

}
