/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import src.api.CategoryManage;
import src.models.Category;

/**
 *
 * @author fxdp
 */
@ApplicationScoped
@Named("categoryCtl")
public class CategoryController {

    @EJB
    CategoryManage categoryManage;

    private String name;

    public String saveForm() {
        categoryManage.add(name);
        return "category_add";
    }
    
    public List<Category> getCategories(){
        return categoryManage.getCategories();
    
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
