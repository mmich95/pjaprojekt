/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controllers;

import javax.ejb.EJB;
import org.omnifaces.cdi.GraphicImageBean;
import src.api.ToolManage;

/**
 *
 * @author fxdp
 */
@GraphicImageBean
public class ImageController {

    @EJB
    private ToolManage toolManage;

    public byte[] get(Long id) {
        return toolManage.getImageBytes(id);
    }

}
