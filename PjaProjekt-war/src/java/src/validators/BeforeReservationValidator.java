/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import src.api.ReservationManage;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
@RequestScoped
@FacesValidator("bfResValidator")
public class BeforeReservationValidator implements Validator  {

    @EJB
    ReservationManage resManage;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }
        
        

        //Leave the null handling of startDate to required="true"
        Object startDateValue = component.getAttributes().get("startDate");
        
        Object showTool = component.getAttributes().get("showTool");
        Tool tool = (Tool) showTool;
        if (startDateValue == null) {
            System.out.println("startDate je null");
            return;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        Date startDate = new Date();
        Date endDate = new Date();
        //data prijdou jako String
        try {
            startDate = dateFormat.parse((String) startDateValue);
            endDate  = (Date) value;
        } catch (ParseException ex) {
            Logger.getLogger(SecondDateValidator.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        Calendar startDateCal = Calendar.getInstance();
        Calendar endDateCal = Calendar.getInstance();
        startDateCal.setTime(startDate);
        endDateCal.setTime(endDate);
        //System.out.println(dateFormat.format(startDateCal.getTime()));
        //System.out.println(dateFormat.format(endDateCal.getTime()));
        //System.out.println(resManage.canBeReserved(startDateCal, endDateCal, tool));
        
        if (!resManage.canBeReserved(startDateCal, endDateCal, tool)) {
            FacesMessage msg = new FacesMessage("Na tento interval rezervace již existuje");
            System.out.println("jiz rezervovano");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);

        }
    }
    
}
