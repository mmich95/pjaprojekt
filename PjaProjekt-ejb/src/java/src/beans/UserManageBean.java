/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import src.api.UserManage;
import src.models.Reservation;
import src.models.User;

/**
 *
 * @author fxdp
 */
@Stateless(name="userManageBean")
public class UserManageBean implements UserManage {
    
    @PersistenceContext(unitName="rentalPers")
    private EntityManager entityManager;

    @Override
    public long addUser(String loginName, String passwordHash, String firstName, String secondName, String telNumber, String email) {
        User newUser = new User(loginName,passwordHash,firstName,secondName,telNumber,email);
        entityManager.persist(newUser);
        return newUser.getId();
    }

    @Override
    public long addUser(String firstName, String secondName, String telNumber, String email) {
        User newUser = new User(firstName,secondName,telNumber,email);
        entityManager.persist(newUser);
        return newUser.getId();
    }

    @Override
    public void deleteUser(long id) {
        entityManager.createQuery("delete from User u where u.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public User getUserById(long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public List<User> getUsers() {
        return (List<User>) entityManager.createQuery("select item from User item").getResultList();
    }
    
}
