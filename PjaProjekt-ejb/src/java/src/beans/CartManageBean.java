/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.beans;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import src.api.CartManage;
import src.api.ReservationManage;
import src.models.Reservation;

/**
 *
 * @author fxdp
 */
@Stateful(name = "cartManageBean")
public class CartManageBean implements CartManage {

    List<Reservation> reservations = new ArrayList<>();

    @EJB
    ReservationManage resManage;

    @Override
    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }

    @Override
    public void removeReservation(Reservation reservation) {
        reservations.remove(reservation);
    }

    @Override
    public List<Reservation> getReservations() {
        return reservations;
    }

    @Remove
    @Override
    public void remove() {
        reservations.forEach((r) -> {
            resManage.delete(r.getId());
        });
        reservations = null;
        System.out.println("Mazu");
    }
    @PreDestroy
    public void preDestroy() {
        reservations.forEach((r) -> {
            resManage.delete(r.getId());
        });
        reservations = null;
        System.out.println("Mazu");
    }

    @Override
    public void discard() {
        reservations.forEach((r) -> {
            resManage.delete(r.getId());
        });
        reservations = new ArrayList<>();
    }

    @Override
    public void afterOrder() {
        reservations.clear();
        System.out.println("Velikost reserv listu je :" + reservations.size());
    }
    

}
