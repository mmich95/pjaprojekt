/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.beans;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import src.api.OrderManage;
import src.api.ReservationManage;
import src.models.Orders;
import src.models.Reservation;
import src.models.User;

/**
 *
 * @author fxdp
 */
@Stateless(name = "orderManageBean")
public class OrderManageBean implements OrderManage {
    
    @PersistenceContext(unitName="rentalPers")
    private EntityManager entityManager;
    @EJB
    private ReservationManage resManage;

    @Override
    public void addOrder(User user, List<Reservation> res, int totalPrice) {
        Orders newOrder = new Orders(user,totalPrice);
        res.forEach((r) -> {
            resManage.setOrder(r, newOrder);
        });
        entityManager.persist(newOrder);
        
        
    }

    @Override
    public void deleteOrder(long id) {
        entityManager.createQuery("delete from Orders o where o.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<Reservation> getReservations() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
