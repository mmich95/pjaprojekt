/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import src.api.CategoryManage;
import src.models.Category;


/**
 *
 * @author fxdp
 */
@Stateless(name = "categoryManageBean")
public class CategoryManageBean  implements CategoryManage{
    
    @PersistenceContext(unitName="rentalPers")
    private EntityManager entityManager;

    @Override
    public void add(String name) {        
        entityManager.persist(new Category(name));
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete from Category c where c.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<Category> getCategories() {
        return (List<Category>) entityManager.createQuery("select item from Category item").getResultList();
    }

    @Override
    public Category getById(long id) {
        return entityManager.find(Category.class, id);
    }
    
}
