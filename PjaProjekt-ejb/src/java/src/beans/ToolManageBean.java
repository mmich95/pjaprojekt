/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import src.api.ToolManage;
import src.models.Category;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
@Stateless(name = "toolManageBean")
public class ToolManageBean implements ToolManage {
    
    @PersistenceContext(unitName="rentalPers")
    private EntityManager entityManager;

    @Override
    public Tool add(String name, String info, byte[] image, Category category, int priceForDay) {
        Tool tool = new Tool(name,info,image,category,priceForDay);
        entityManager.persist(tool);
        System.out.println(tool.getName());
        System.out.println(tool.getInfo());
        System.out.println(tool.getPriceForDay());
        System.out.println(tool.getCategory().getName());
        return tool;    
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete from Tool t where t.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }
    
    @Override
    public List<Tool> getTools() {
        return (List<Tool>) entityManager.createQuery("select item from Tool item").getResultList();
    }

    @Override
    public byte[] getImageBytes(long id) {
        return entityManager.find(Tool.class, id).getImage();
    }

    @Override
    public Tool getToolById(long id) {
        return entityManager.find(Tool.class, id);
    }

    @Override
    public List<Tool> getToolsByCategory(Category cat) {
        return (List<Tool>) entityManager.createQuery("select item from Tool item where item.category = :id")
                .setParameter("id", cat)
                .getResultList();
    }
    
}
