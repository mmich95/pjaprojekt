/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import src.api.ReservationManage;
import src.models.Orders;
import src.models.Reservation;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
@Stateless(name = "reservationManageBean")
public class ReservationManageBean implements ReservationManage {

    @PersistenceContext(unitName = "rentalPers")
    private EntityManager entityManager;

    @Override
    public long add(Calendar dateSince, Calendar dateTo, Tool tool, int priceTotal) {
        Reservation res = new Reservation(dateSince, dateTo, tool, priceTotal);
        entityManager.persist(res);
        return res.getId();
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete from Reservation r where r.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<Reservation> getReservations() {
        return (List<Reservation>) entityManager.createQuery("select item from Reservation item").getResultList();
    }

    @Override
    public Reservation getById(long id) {
        return entityManager.find(Reservation.class, id);
    }

    @Override
    public Calendar getSinceDateByTool(Tool tool) {
        Calendar actualDate = Calendar.getInstance();
        Calendar previousSince = Calendar.getInstance();
        List<Reservation> res = entityManager.createQuery("select r from Reservation r where r.tool = :tool AND r.dateTo > :actualDate order by r.dateTo")
                .setParameter("tool", tool)
                .setParameter("actualDate", actualDate)
                .getResultList();

        if (res.isEmpty()) {
            return actualDate;
        } else {
            for (Reservation r : res) {
                if (!previousSince.before(r.getDateSince())) {
                    previousSince = r.getDateTo();
                }
            }
            return previousSince;

        }

    }

    @Override
    public Calendar getToDateByTool(Tool tool) {
        Calendar sinceDate = this.getSinceDateByTool(tool);
        List<Reservation> res = entityManager.createQuery("select r from Reservation r where r.tool = :tool AND r.dateSince > :since order by r.dateSince")
                .setParameter("tool", tool).setParameter("since", sinceDate).getResultList();
        if (res.isEmpty()) {
            return new GregorianCalendar(2999, 0, 31);
        } else {
            return res.get(0).getDateSince();
        }
    }

    @Override
    public void setOrder(Reservation res, Orders order) {

        Reservation reserv = entityManager.find(Reservation.class, res.getId());
        reserv.setOrder(order);
        entityManager.merge(reserv);

    }

    @Override
    public boolean isRented(Tool tool) {
        List<Reservation> res = entityManager.createQuery("select r from Reservation r where r.tool = :tool")
                .setParameter("tool", tool).getResultList();

        boolean rented = false;

        Calendar actualDate = Calendar.getInstance();

        for (Reservation r : res) {

            if (actualDate.after(r.getDateSince()) && actualDate.before(r.getDateTo())) {
                rented = true;
            }
        }
        return rented;
    }

    @Override
    public List<Reservation> getReservationsByTool(Tool tool) {
        return (List<Reservation>) entityManager
                .createQuery("select r from Reservation r where r.tool = :tool")
                .setParameter("tool", tool).getResultList();
    }

    @Override
    public boolean canBeReserved(Calendar firstDate, Calendar secondDate,Tool tool) {
        boolean canBe = true;
        List<Reservation> res = this.getReservationsByTool(tool);
        //System.out.println("Pocet rezervaci na tento nastroj je: " + res.size());

        for (Reservation r : res) {
            //System.out.println("Datum od z db: " + r.getDateSince().getTime());
            //System.out.println("Datum do z db: " + r.getDateTo().getTime());
            if (firstDate.after(r.getDateSince()) && firstDate.before(r.getDateTo())) {
                canBe = false;
            }
            if (firstDate.equals(r.getDateSince())) {
                canBe = false;
            }
            if (secondDate.after(r.getDateSince()) && secondDate.before(r.getDateTo())) {
                canBe = false;
            }
            if (secondDate.equals(r.getDateTo())) {
                canBe = false;
            }

        }
        return canBe;
    }

}
