package src.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * POJO reprezentujici uzivatele
 */
@Entity
@Table(name = "tool")
@XmlRootElement
public class Tool implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "info")
    private String info;
    
    @Column(name = "image")
    private byte[] image;

    @Column(name = "price_day")
    private int priceForDay;

    @JoinColumn(name = "category_id")
    @ManyToOne
    private Category category;

    public Tool() {

    }

    public Tool(String name, String info, byte[] image, Category category, int priceForDay) {
        this.image = image;
        this.name = name;
        this.info = info;
        this.category = category;
        this.priceForDay = priceForDay;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getPriceForDay() {
        return priceForDay;
    }

    public void setPriceForDay(int priceForDay) {
        this.priceForDay = priceForDay;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tool)) {
            return false;
        }
        Tool other = (Tool) object;
        if (this.name != null || other.name == null && (this.name == null || this.name.equals(other.name))) {
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "src.Post[ id=" + id + " ]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
