/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author fxdp
 */
@Entity
@Table(name = "reservation")
public class Reservation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date_since")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dateSince;

    @Column(name = "date_to")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dateTo;

    @JoinColumn(name = "tool_id")
    @ManyToOne
    private Tool tool;
    
    @JoinColumn(name = "order_id")
    @ManyToOne
    private Orders order;
    
    @Column(name = "price_total")
    private int priceTotal;

    public Reservation() {
    }

    public Reservation(Calendar dateSince, Calendar dateTo, Tool tool, int priceTotal) {
        this.dateSince = dateSince;
        this.dateTo = dateTo;
        this.tool = tool;
        this.priceTotal = priceTotal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation) object;
        if (this.id != null || other.id == null && (this.id == null || this.id.equals(other.id))) {
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reservation with id: " + id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getDateSince() {
        return dateSince;
    }

    public void setDateSince(Calendar dateSince) {
        this.dateSince = dateSince;
    }

    public Calendar getDateTo() {
        return dateTo;
    }

    public void setDateTo(Calendar dateTo) {
        this.dateTo = dateTo;
    }

    public Tool getTool() {
        return tool;
    }

    public void setTool(Tool tool) {
        this.tool = tool;
    }

    public int getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(int priceTotal) {
        this.priceTotal = priceTotal;
    }
    
    public String getSinceDateInString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        dateFormat.setTimeZone(this.dateSince.getTimeZone());
        return dateFormat.format(this.dateSince.getTime());
    }
    
    public String getToDateInString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
        dateFormat.setTimeZone(this.dateTo.getTimeZone());
        return dateFormat.format(this.dateTo.getTime());
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }
    
    

}
