/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.api;

import java.util.List;
import src.models.Reservation;
import src.models.User;

/**
 *
 * @author fxdp
 */
public interface OrderManage {
    public void addOrder(User user, List<Reservation> res,int totalPrice);
    public void deleteOrder(long id);
    public List<Reservation> getReservations();
}
