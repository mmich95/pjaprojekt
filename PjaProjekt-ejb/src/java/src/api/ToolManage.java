/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.api;

import java.util.List;
import src.models.Category;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
public interface ToolManage {
    public Tool add(String name, String info, byte[] image, Category category, int priceForDay);
    public void delete(long id);
    public List<Tool> getTools();
    public List<Tool> getToolsByCategory(Category cat);
    public byte[] getImageBytes(long id);
    public Tool getToolById(long id);
}
