/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.api;

import java.util.List;
import src.models.Reservation;

/**
 *
 * @author fxdp
 */
public interface CartManage {
    
    public void addReservation(Reservation reservation);
    public void removeReservation(Reservation reservation);
    public List<Reservation> getReservations();
    public void remove();
    public void discard();
    public void afterOrder();
    
}
