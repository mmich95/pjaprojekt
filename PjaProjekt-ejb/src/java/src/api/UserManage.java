/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.api;

import java.util.List;
import src.models.User;

/**
 *
 * @author fxdp
 */
public interface UserManage {

    public long addUser(String loginName, String passwordHash, String firstName, String secondName, String telNumber, String email);

    public long addUser(String firstName, String secondName, String telNumber, String email);

    public void deleteUser(long id);

    public User getUserById(long id);

    public List<User> getUsers();

}
