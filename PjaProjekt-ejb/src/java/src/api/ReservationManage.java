/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.api;

import java.util.Calendar;
import java.util.List;
import src.models.Orders;
import src.models.Reservation;
import src.models.Tool;

/**
 *
 * @author fxdp
 */
public interface ReservationManage {

    public long add(Calendar dateSince, Calendar dateTo, Tool tool, int priceTotal);
    public void delete(long id);
    public List<Reservation> getReservations();
    public Reservation getById(long id);
    public Calendar getSinceDateByTool(Tool tool);
    public Calendar getToDateByTool(Tool tool);
    public void setOrder(Reservation res, Orders order);
    public boolean isRented(Tool tool);
    public List<Reservation> getReservationsByTool(Tool tool);
    public boolean canBeReserved(Calendar firstDate, Calendar secondDate,Tool tool);
    

}
