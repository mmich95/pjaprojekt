/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.api;

import java.util.List;
import src.models.Category;

/**
 *
 * @author fxdp
 */
public interface CategoryManage {
    
    public void add(String name);
    public void delete(long id);
    public Category getById(long id);
    public List<Category> getCategories();
    
    
}
